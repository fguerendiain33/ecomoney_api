import * as express from 'express';
import { Express } from 'express';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as config from './config';


const APIRESTVERSION = config.get("apiRestVersion");

export function build():Express{
 
    let app = express();

    app.use(bodyParser.json());
    app.use(cors());



    //URLs DEFINITIVAS
//    app.get('/v'+APIRESTVERSION.prod+'/informeBacklogDiario', informeBacklogDiario.backlogDiario); // [RUN] genera backlog
//    app.post('/v'+APIRESTVERSION.prod+'/informeBacklogDiario', informeBacklogDiario.backlogDiario); // [RUN] genera backlog y guarda en BBDD local

   
    return app;
}