import { User} from "./User";

export class Acount {

    constructor(
            private name : string,
            private type : string,
            private owner : User
        ){
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getType(): string {
        return this.type;
    }

    public setType(type: string): void {
        this.type = type;
    }

    public getOwner(): User {
        return this.owner;
    }

    public setOwner(owner: User): void {
        this.owner = owner;
    }



}
