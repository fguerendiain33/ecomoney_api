export class User{

    constructor(
            private name : string, 
            private age : number, 
            private mail : string = ''
        ){}

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getAge(): number {
        return this.age;
    }

    public setAge(age: number): void {
        this.age = age;
    }

    public getMail(): string {
        return this.mail;
    }

    public setMail(mail: string): void {
        this.mail = mail;
    }
}