import { isNullOrUndefined } from 'util';
/**
 * movimiento economico
 * param => date: fecha en que se relaiza el movimiento
 * param => type: si es un ingreso de valor o un egreso
 * param => value: valor numerico del movimiento
 * param => acount: cuenta en el que se realiza el movimiento 
 */

export class Transaction{

    constructor(
            private type : string,
            private acount : Acount,
            private value : number,
            private date : Date = new Date()
        ){};

    public getType(): string {
        return this.type;
    }

    public setType(type: string): void {
        this.type = type;
    }

    public getAcount(): Acount {
        return this.acount;
    }

    public setAcount(acount: Acount): void {
        this.acount = acount;
    }

    public getValue(): number {
        return this.value;
    }

    public setValue(value: number): void {
        this.value = value;
    }

    public getDate(): Date{
        return this.date;
    }

    public setDate(date: Date): void {
        this.date = date;
    }





}
