"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http = require("http");
var config = require("./config");
var APIFactory = require("./APIFactory");
var log = require("./logger");
var apiPort = +config.get('apiPort');
var api = APIFactory.build();
var server = http.createServer(api);
server.listen(apiPort);
log.info('SERVER', "Ready: Listening on port " + apiPort);
//# sourceMappingURL=server.js.map