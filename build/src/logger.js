"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
var config = require("./config");
var fs = require("fs");
var util_1 = require("util");
var FILELOCATION = config.get('logs').logsavelocation;
var FILEEXTENSION = config.get('logs').logfileextension;
var LOGFILENAME = config.get("logs").logsavefilename;
var PRINTLENGTH = config.get("logs").printLength;
function info(tag, msg, filename) {
    if (util_1.isNullOrUndefined(filename)) {
        log('INFO', tag, msg);
    }
    else {
        log('INFO', tag, msg, filename);
    }
}
exports.info = info;
function infoFullMessage(tag, msg, filename, cutLog) {
    log('INFO', tag, msg, filename, cutLog);
}
exports.infoFullMessage = infoFullMessage;
function error(tag, msg, filename) {
    if (util_1.isNullOrUndefined(filename)) {
        log('ERROR', tag, msg);
    }
    else {
        log('ERROR', tag, msg, filename);
    }
}
exports.error = error;
function awaitAsyncErrorHandler(msg, filename) {
    if (util_1.isNullOrUndefined(filename)) {
        log('ERROR', 'PROMISE', msg);
    }
    else {
        log('ERROR', 'PROMISE', msg, filename);
    }
}
exports.awaitAsyncErrorHandler = awaitAsyncErrorHandler;
function taskBeginInfo(msg, filename) {
    if (util_1.isNullOrUndefined(filename)) {
        log('INFO', 'TASK', ' --COMIENZA EJECUCION DE TAREA: ' + msg);
    }
    else {
        log('INFO', 'TASK', ' --COMIENZA EJECUCION DE TAREA: ' + msg, filename);
    }
}
exports.taskBeginInfo = taskBeginInfo;
function taskEndInfo(msg, filename) {
    if (util_1.isNullOrUndefined(filename)) {
        log('INFO', 'TASK', ' --FINALIZA EJECUCION DE TAREA: ' + msg);
    }
    else {
        log('INFO', 'TASK', ' --FINALIZA EJECUCION DE TAREA: ' + msg, filename);
    }
}
exports.taskEndInfo = taskEndInfo;
function minimal(msg, filename) {
    logMinimal(msg, filename);
}
exports.minimal = minimal;
function logMinimal(msg, filename) {
    var message = msg;
    console.log(message);
    if (!util_1.isNullOrUndefined(filename)) {
        logFileWriter(filename, message);
    }
    ;
}
function log(type, tag, msg, filename, cutLog) {
    var date = moment().format('YYYY.MM.DD HH:mm:ss.SS') + ': ';
    type = '[' + type + '] ';
    tag = tag + ': ';
    var mesAux = msg;
    if (util_1.isNullOrUndefined(cutLog) || cutLog == false) {
        mesAux = cutLogString(msg);
    }
    var message = date + type + tag + mesAux;
    console.log(message);
    if (util_1.isNullOrUndefined(filename)) {
        logFileWriter(LOGFILENAME, message);
    }
    else {
        logFileWriter(filename, message);
        logFileWriter(LOGFILENAME, message);
    }
    ;
}
function logFileWriter(filename, msg) {
    var file = FILELOCATION + filename + '_' + moment().format('YYYY-MM-DD') + '.' + FILEEXTENSION;
    var infoStream = fs.createWriteStream(file, { flags: 'a' });
    infoStream.write(msg + '\r\n');
}
function cutLogString(v_string) {
    if (v_string.length > PRINTLENGTH) {
        var endOfString = '   ... (continua con ' + (v_string.length - PRINTLENGTH) + ' caracteres)';
        v_string = v_string.substring(0, PRINTLENGTH) + endOfString;
    }
    return v_string;
}
//# sourceMappingURL=logger.js.map