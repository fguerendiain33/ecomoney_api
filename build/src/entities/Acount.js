"use strict";
var Acount = /** @class */ (function () {
    function Acount(name, type, owner) {
        this.name = name;
        this.type = type;
        this.owner = owner;
    }
    Acount.prototype.getName = function () {
        return this.name;
    };
    Acount.prototype.setName = function (name) {
        this.name = name;
    };
    Acount.prototype.getType = function () {
        return this.type;
    };
    Acount.prototype.setType = function (type) {
        this.type = type;
    };
    Acount.prototype.getOwner = function () {
        return this.owner;
    };
    Acount.prototype.setOwner = function (owner) {
        this.owner = owner;
    };
    return Acount;
}());
//# sourceMappingURL=Acount.js.map