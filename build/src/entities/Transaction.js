"use strict";
/**
 * movimiento economico
 * param => date: fecha en que se relaiza el movimiento
 * param => type: si es un ingreso de valor o un egreso
 * param => value: valor numerico del movimiento
 * param => acount: cuenta en el que se realiza el movimiento
 */
var Transaction = /** @class */ (function () {
    function Transaction(type, acount, value, date) {
        this.type = type;
        this.acount = acount;
        this.value = value;
        if (date == null) {
            this.date = new Date();
        }
        else {
            this.date = date;
        }
    }
    Transaction.prototype.getType = function () {
        return this.type;
    };
    Transaction.prototype.setType = function (type) {
        this.type = type;
    };
    Transaction.prototype.getAcount = function () {
        return this.acount;
    };
    Transaction.prototype.setAcount = function (acount) {
        this.acount = acount;
    };
    Transaction.prototype.getValue = function () {
        return this.value;
    };
    Transaction.prototype.setValue = function (value) {
        this.value = value;
    };
    Transaction.prototype.getDate = function () {
        return this.date;
    };
    Transaction.prototype.setDate = function (date) {
        this.date = date;
    };
    return Transaction;
}());
//# sourceMappingURL=Transaction.js.map