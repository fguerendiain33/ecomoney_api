"use strict";
var User = /** @class */ (function () {
    function User(name, age, mail) {
        this.name = name;
        this.age = age;
        if (mail != null) {
            this.mail = mail;
        }
    }
    User.prototype.getName = function () {
        return this.name;
    };
    User.prototype.setName = function (name) {
        this.name = name;
    };
    User.prototype.getAge = function () {
        return this.age;
    };
    User.prototype.setAge = function (age) {
        this.age = age;
    };
    User.prototype.getMail = function () {
        if (this.mail != null) {
            return this.mail;
        }
        return '';
    };
    User.prototype.setMail = function (mail) {
        this.mail = mail;
    };
    return User;
}());
//# sourceMappingURL=User.js.map