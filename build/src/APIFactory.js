"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
var config = require("./config");
var APIRESTVERSION = config.get("apiRestVersion");
function build() {
    var app = express();
    app.use(bodyParser.json());
    app.use(cors());
    //URLs DEFINITIVAS
    //    app.get('/v'+APIRESTVERSION.prod+'/informeBacklogDiario', informeBacklogDiario.backlogDiario); // [RUN] genera backlog
    //    app.post('/v'+APIRESTVERSION.prod+'/informeBacklogDiario', informeBacklogDiario.backlogDiario); // [RUN] genera backlog y guarda en BBDD local
    return app;
}
exports.build = build;
//# sourceMappingURL=APIFactory.js.map